    #include <stdio.h>
    #include <stdlib.h>
    #include <errno.h>
    #include <unistd.h>
    #include <pthread.h>
    #include <semaphore.h>
    #include "graphic.h"
     
    #define N 200
    #define NB_THREAD 12
     
    typedef struct {
    	int x,y,color;
    } Point;
   // sem_t semaphore;
    pthread_mutex_t mutex;
    Point points[N];
    int n=N/NB_THREAD;

    void* fils(void* thread_id){///
	long tnumber = (long) thread_id; 
  	pthread_mutex_lock(&mutex);
	int min=i*n;
	while (True){
		int ppid= getppid();
		//int pid= getpid();
		if (ppid == 1){ exit(0);}
		//p(sem)	
		//sem_wait(&semaphore);
		for(int j=min;j<(min+n);++j){
			points[j].x+=g_random()%2?1:-1;
		    	points[j].y+=g_random()%2?1:-1;
			}
		//v(sem)
		//sem_post(&semaphore);
		g_msleep(20);
	}
	pthread_mutex_unlock(&mutex);//////////
    }


    int main() {
    	
    	int i,success;
        //sem <- semaphore POSIX
	//shm <- segment de mémoire partagée contenant le tableau des N points
	pthread_t threads[NB_THREAD];
	pthread_mutex_init(&mutex,NULL);
	//sem_init(&semaphore, PTHREAD_PROCESS_SHARED, 1);
    	/* initialisation aléatoire des N points */
     
    	for(i=0; i<N; ++i) {
    		points[i].x=g_random()%(G_WIDTH-200)+100;
    		points[i].y=g_random()%(G_HEIGHT-200)+100;
    		points[i].color=g_random()%G_NB_COLORS;
    	}
     
    	g_init();

	for (i=0;i<NB_THREAD;++i){     
		// crée fil
		success = pthread_create(&threads[i],NULL,fils,(void*)i);//////
	    	if (success != 0) {
	      		printf("ERROR: Unable to create worker thread %ld successfully\n",i);
		pthread_mutex_destroy(&mutex);
	      return 1;}
		fils(i,points);////////
		}
	for(i=0; i<NB_THREAD; ++i) {
	    pthread_join(threads[i],NULL);
	  }
    	while(1) {
    		/*// mise à jour des points
    		for(i=0; i<N; ++i) {
    			points[i].x+=g_random()%2?1:-1;
    			points[i].y+=g_random()%2?1:-1;
    		}*/

		//p(sem)
     		//sem_wait(&semaphore);
    		/* Affichage des points */
    		g_clear();
    		for(i=0; i<N; ++i) {
   			g_draw(points[i].x, points[i].y, points[i].color);
		 }
     
    		g_flush();
		//v(sem)
	//	sem_post(&semaphore);
    		g_msleep(20);
    	}
     	//sem_destroy(&semaphore);
	pthread_mutex_destroy(&mutex);
    	return 0;
    }



