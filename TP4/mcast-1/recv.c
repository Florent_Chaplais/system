#define GROUP "230.0.0.0"
#define PORT 5000

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

////// structure mreqn
	struct ip_mreqn {
		struct in_addr imr_multiaddr; /* IP du groupe */
		struct in_addr imr_address;   /* IP iface locale */
		int imr_ifindex;   			/* Numéro d'interface */
	};


int main(int argc, char const *argv[])
{
		

/* Création de la socket IPv4 */
  if((sdr=socket(AF_INET, SOCK_DGRAM, 0))==-1) {
    perror("socket"); exit(1);
  }

////// init structure ip_mreqn
	struct ip_mreqn mreqn;
	 
	if(inet_aton(GROUP, &mreqn.imr_multiaddr)==0) {
		fprintf(stderr,"Pb Adr multicast : %s !\n", GROUP);
		exit(1);
	}
	 
	mreqn.imr_address.s_addr = htonl(INADDR_ANY);
	mreqn.imr_ifindex=0; /* n'importe quelle interface */
	 
	if (setsockopt(sdr, IPPROTO_IP, IP_ADD_MEMBERSHIP, 
				(void *) &mreqn, sizeof(struct ip_mreqn)) < 0)
		perror("setsockopt – IP_ADD_MEMBERSHIP");

	if(bind(sdr, (struct sockaddr *)&addr, sizeof addr)) {
    perror("bind"); exit(1);
  }

////// joindre groupe mulitcast
	if (setsockopt(sdr, IPPROTO_IP, IP_ADD_MEMBERSHIP, 
				(void *) &mreqn, sizeof(struct ip_mreqn)) < 0)
		perror("setsockopt – IP_ADD_MEMBERSHIP");

////// quitter le groupe
	if(setsockopt(sdr, IPPROTO_IP, IP_DROP_MEMBERSHIP,
			&mreqn, 	sizeof(mreqn)) == -1)
			perror("setsockopt: IP_DROP_MEMBERSHIP");

///// traitement
	while(1) {
	 
	    /* Attente et lecture d'une requête */
	    len_src_addr=sizeof src_addr;
	    if((ret=recvfrom(sdr, request, DGRAM_MAX-1, 0,
	        (struct sockaddr*) &src_addr, &len_src_addr))==-1) {
	      perror("recvfrom"); exit(1);
	    }
	    request[ret]=0;
	 
	    /* traitement de la requête(passage en majuscule) */
	    {
	      print("\nx: %d, y: %d, color: %s",request[0],request[1],request[2]);
	      }
    }
	
	return 0;
}