#define GROUP "230.0.0.0"
#define PORT 5000

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

int main(int argc, char const *argv[])
{
////// verif nb arg
	if(arg != 3)
		perror("nb arg"); exit(1);
	
	/* Création de la socket IPv4 */
	if((sd=socket(AF_INET, SOCK_DGRAM, 0))==-1) {
		perror("socket"); exit(1);
	}
////// fixer champ TTL
	int ttl=2;
	if(setsockopt(sd, IPPROTO_IP, IP_MULTICAST_TTL, 
		(void *) &ttl, sizeof(ttl)) == -1)
	   perror("set IP_MULTICAST_TTL");

////// empecher emission vers emetteur
	int loopback=0;
	if(setsockopt(sd, IPPROTO_IP, IP_MULTICAST_LOOP, 
		(void *) &loopback, sizeof(loopback)) == -1)
	   perror("set IP_MULTICAST_LOOP");

///// emission
	if(send(sd,argv,strlen(argv)+1,0)
		perror("send"); exit(1);
	
	return 0;
}